﻿using System.ComponentModel.DataAnnotations;

namespace SchoolJournal.Server.DTO
{
    public class LoginDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}

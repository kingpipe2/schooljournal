import { Injectable } from '@angular/core';
import { RestService } from './rest-service.service';
import { Grade } from '../models/grade';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GradeService extends RestService<Grade> {

  constructor(private http: HttpClient) {
    super(http, 'grades/');
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestService } from './rest-service.service';
import { Subject } from '../models/subject';

@Injectable({
  providedIn: 'root'
})
export class SubjectService extends RestService<Subject> {

  constructor(private http: HttpClient) {
    super(http, 'subjects/');
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestService } from './rest-service.service';
import { Student } from '../models/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService extends RestService<Student> {

  constructor(private http: HttpClient) {
    super(http, 'students/');
  }
}

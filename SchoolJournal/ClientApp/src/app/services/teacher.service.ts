import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestService } from './rest-service.service';
import { Teacher } from '../models/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherService extends RestService<Teacher> {

  constructor(private http: HttpClient) {
    super(http, 'teachers/');
  }
}

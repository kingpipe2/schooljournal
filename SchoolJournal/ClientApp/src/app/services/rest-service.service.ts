import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse  } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';

export abstract class RestService<T> {

  protected url: string;

  constructor(protected _http: HttpClient, protected actionUrl:string){

    this.url = environment.baseUrl + actionUrl;
  }

  GetAll():Observable<T[]> {
    return this._http.get(this.url) as Observable<T[]>;
  }

  Get(id:number):Observable<T> {
    return this._http.get(`${this.url}${id}`) as Observable<T>;
  }

  async Post(data: T){
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    return await this._http.post(this.url, data, {headers: headerOptions}).toPromise();
  }

  Put(data: T, id:number){
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._http.put(`${this.url}${id}`, data, {headers: headerOptions}).toPromise();
  }

  async Delete(id: number){
    return await this._http.delete(`${this.url}${id}`).toPromise();
  }
}

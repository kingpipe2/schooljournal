import { Injectable } from '@angular/core';
import { RestService } from './rest-service.service';
import { School } from '../models/school';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SchoolService extends RestService<School> {

  constructor(private http: HttpClient) {
    super(http, 'schools/');
  }
}

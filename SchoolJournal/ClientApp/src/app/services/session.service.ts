import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { UserLogin } from '../models/userLogin';
import { UserRegister } from '../models/userRegister';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { NavbarService } from './navbar.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

    protected url: string;
    public isAutorization = false;
    headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });

    constructor(protected _http: HttpClient, private router: Router, private navbar: NavbarService) {
      this.url = environment.baseUrl + 'account';
    }

    public isAuthenticated(): Observable<boolean> {
        return of(this.isAutorization);
    }

    public login(userLogin: UserLogin) {
        return this._http.post(this.url + '/login', userLogin, {headers: this.headerOptions}).subscribe(res => {
            this.isAutorization = true;
            this.navbar.show();
            this.router.navigate(['/schools']);
          });
    }

    public register(userRegister: UserRegister) {
       return this._http.post(this.url + '/register', userRegister, {headers: this.headerOptions}).subscribe(res => {
          this.isAutorization = true;
          this.navbar.show();
          this.router.navigate(['/schools']);
        });
    }

    public logout() {
      return this._http.post(this.url + '/logout', null, {headers: this.headerOptions}).subscribe(res => {
          this.isAutorization = false;
          this.navbar.hide();
          this.router.navigate(['/login']);
        });
    }
}

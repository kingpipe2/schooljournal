export class EnumHelper
{
     public static enumSelector(definition): EnumElement[] {
        return Object.keys(definition).filter(f => !isNaN(Number(f)))
          .map(key => ({ value: definition[key], key: +key } as EnumElement));
      }
}

 export interface EnumElement{
    key: number;
    value: string;
  }
  
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatTableModule, MatButtonModule,
         MatSidenavModule, MatIconModule, MatListModule,
         MatInputModule, MatDialogModule, MatCardModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import { LayoutComponent } from './layout/layout.component';
import { GradeComponent } from './componets/grade/grade.component';
import { GradeListComponent } from './componets/grade/grade-list/grade-list.component';
import { SchoolComponent } from './componets/school/school.component';
import { SchoolListComponent } from './componets/school/school-list/school-list.component';
import { StudentComponent } from './componets/student/student.component';
import { StudentListComponent } from './componets/student/student-list/student-list.component';
import { SubjectComponent } from './componets/subject/subject.component';
import { SubjectListComponent } from './componets/subject/subject-list/subject-list.component';
import { TeacherComponent } from './componets/teacher/teacher.component';
import { TeacherListComponent } from './componets/teacher/teacher-list/teacher-list.component';
import { ModalWindowComponent } from './modal-window/modal-window.component';
import { LoginComponent } from './componets/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { CommonModule } from '@angular/common';
import { SignUpComponent } from './componets/sign-up/sign-up.component';

@NgModule({
  declarations: [
    GradeComponent,
    GradeListComponent,
    SchoolComponent,
    SchoolListComponent,
    StudentComponent,
    StudentListComponent,
    SubjectComponent,
    SubjectListComponent,
    TeacherComponent,
    TeacherListComponent,
    LayoutComponent,
    ModalWindowComponent,
    LoginComponent,
    SignUpComponent,
  ],
  imports: [
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    MatDialogModule,
    MatCardModule,
    CommonModule
  ],
  entryComponents: [
    ModalWindowComponent,
    SubjectComponent,
    GradeComponent,
    SchoolComponent,
    StudentComponent,
    TeacherComponent
  ],
  providers: [
    AuthGuard
  ],
  bootstrap: [LayoutComponent]
})
export class AppModule { }

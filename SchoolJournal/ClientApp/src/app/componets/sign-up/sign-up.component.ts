import { Component, OnInit } from '@angular/core';
import { UserRegister } from '../../models/userRegister';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  public user: UserRegister = <UserRegister>{};

  constructor(private sessionService: SessionService) { }

  public ngOnInit() {
  }

  public register() {
    this.sessionService.register(this.user);
  }

}

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Teacher } from '../../../models/teacher';
import { TeacherService } from '../../../services/teacher.service';
import { Gender } from '../../../models/base/gender';
import { ModalWindowComponent } from '../../../modal-window/modal-window.component';
import { MatDialog } from '@angular/material';
import { TeacherComponent } from '../teacher.component';

@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  styleUrls: ['./teacher-list.component.css']
})
export class TeacherListComponent implements OnInit {

  Gender = Gender;

  displayedColumns = ['firstName', 'middleName', 'lastName', 'homeAddress',
                      'dateOfBirth', 'gender', 'subjectName', 'schoolName', 'actions'];
  dataSource: Observable<Teacher[]>;

  constructor( private teacherService: TeacherService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.updateTable();
  }
  addTeacher() {
    const teacher = new Teacher();
    const dialogRef = this.dialog.open(TeacherComponent, {data: teacher});
    dialogRef.afterClosed().subscribe(() => this.updateTable());
  }

  editTeacher(teacher: Teacher) {
    const dialogRef = this.dialog.open(TeacherComponent, {data: teacher});
    dialogRef.afterClosed().subscribe(() => this.updateTable());
  }

  deleteTeacher(id: number) {
    const dialogRef = this.dialog.open(ModalWindowComponent);

    dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.teacherService.Delete(id).then(answer => this.updateTable());
            }
        });
  }

  private updateTable() {
    this.dataSource = this.teacherService.GetAll();
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { EnumElement, EnumHelper } from '../../helper/enum-helper';
import { Gender } from '../../models/base/gender';
import { Subject } from '../../models/subject';
import { School } from '../../models/school';
import { TeacherService } from '../../services/teacher.service';
import { SchoolService } from '../../services/school.service';
import { SubjectService } from '../../services/subject.service';
import { NgForm } from '@angular/forms';
import { Teacher } from '../../models/teacher';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  selectedGender: string;
  gender: EnumElement[];
  subjects: Subject[];
  schools: School[];

  constructor(private teacherService: TeacherService,
              private schoolService: SchoolService,
              private subjectService: SubjectService,
              @Inject(MAT_DIALOG_DATA) public teacher: Teacher,
              public dialogRef: MatDialogRef<TeacherComponent>) {
  }

  ngOnInit() {
    this.gender = EnumHelper.enumSelector(Gender);
    this.subjectService.GetAll().subscribe(subjects => {this.subjects = subjects as Subject[]; });
    this.schoolService.GetAll().subscribe(schools => {this.schools = schools as School[]; });

  }

  async onSubmit(form: NgForm) {
    form.value.id = this.teacher.id;
    if (form.value.id === undefined) {
      await this.teacherService.Post(form.value as Teacher);
    } else {
      await this.teacherService.Put(form.value as Teacher, form.value.id);
    }
    this.dialogRef.close();
  }
}

import { Component, OnInit } from '@angular/core';
import { School } from '../../../models/school';
import { Observable } from 'rxjs';
import { ModalWindowComponent } from '../../../modal-window/modal-window.component';
import { MatDialog } from '@angular/material';
import { SchoolComponent } from '../school.component';
import { SchoolService } from '../../../services/school.service';

@Component({
  selector: 'app-school-list',
  templateUrl: './school-list.component.html',
  styleUrls: ['./school-list.component.css']
})
export class SchoolListComponent implements OnInit {

  displayedColumns = ['name', 'area', 'region', 'town', 'street', 'phone', 'indexMail', 'mail', 'actions'];
  dataSource: Observable<School[]>;

   constructor( private schoolService: SchoolService, public dialog: MatDialog) {
  }

  addSchool() {
    const school = new School();
    const dialogRef = this.dialog.open(SchoolComponent, {data: school});
    dialogRef.afterClosed().subscribe(() => this.updateTable());
  }

  editSchool(school: School) {
    const dialogRef = this.dialog.open(SchoolComponent, {data: school});
    dialogRef.afterClosed().subscribe(() => this.updateTable());
  }


  deleteSchool(id: number) {
    const dialogRef = this.dialog.open(ModalWindowComponent);

    dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.schoolService.Delete(id).then(answer => this.updateTable());
            }
        });
  }

  ngOnInit() {
    this.dataSource = this.schoolService.GetAll();
  }

   updateTable() {
    this.dataSource = this.schoolService.GetAll();
  }
}

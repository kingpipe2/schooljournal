import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { School } from '../../models/school';
import { SchoolService } from '../../services/school.service';

@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  styleUrls: ['./school.component.css']
})
export class SchoolComponent implements OnInit {

  constructor(private schoolService: SchoolService,
              @Inject(MAT_DIALOG_DATA) public school: School,
              public dialogRef: MatDialogRef<SchoolComponent>) { }

  async onSubmit(form: NgForm) {
    form.value.id = this.school.id;
    if (form.value.id === undefined) {
      await this.schoolService.Post(form.value as School);
    } else {
      await this.schoolService.Put(form.value as School, form.value.id);
    }
    this.dialogRef.close();
  }

  ngOnInit() {
  }
}

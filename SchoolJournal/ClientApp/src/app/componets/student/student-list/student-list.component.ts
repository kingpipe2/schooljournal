import { Component, OnInit } from '@angular/core';
import { Student } from '../../../models/student';
import { Observable } from 'rxjs';
import { Gender } from '../../../models/base/gender';
import { MatDialog } from '@angular/material';
import { StudentComponent } from '../student.component';
import { ModalWindowComponent } from '../../../modal-window/modal-window.component';
import { StudentService } from '../../../services/student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  Gender = Gender;

  displayedColumns = ['firstName', 'middleName', 'lastName', 'homeAddress', 'dateOfBirth', 'gender', 'gradeName', 'actions'];
  dataSource: Observable<Student[]>;

   constructor( private studentService: StudentService, public dialog: MatDialog) {
  }

  addStudent() {
    const student = new Student();
    const dialogRef = this.dialog.open(StudentComponent, {data: student});
    dialogRef.afterClosed().subscribe(() => this.updateTable());
  }

  editStudent(student: Student) {
    const dialogRef = this.dialog.open(StudentComponent, {data: student});
    dialogRef.afterClosed().subscribe(() => this.updateTable());
  }

  deleteStudent(id: number) {
    const dialogRef = this.dialog.open(ModalWindowComponent);

    dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.studentService.Delete(id).then(answer => this.updateTable());
            }
        });
  }
  ngOnInit() {
    this.updateTable();
   }


  private updateTable() {
    this.dataSource = this.studentService.GetAll();
  }
}

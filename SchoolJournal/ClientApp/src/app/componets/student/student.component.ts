import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Gender } from '../../models/base/gender';
import { Grade } from '../../models/grade';
import { EnumHelper, EnumElement } from '../../helper/enum-helper';
import { Student } from '../../models/student';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { StudentService } from '../../services/student.service';
import { GradeService } from '../../services/grate.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  gender: EnumElement[];
  selectedGender: any;
  grades: Grade[];

  constructor(private studentService: StudentService,
              private gradeService: GradeService ,
              @Inject(MAT_DIALOG_DATA) public student: Student,
              public dialogRef: MatDialogRef<StudentComponent>) {
    this.gender = EnumHelper.enumSelector(Gender);
  }

  ngOnInit() {
    this.gender = EnumHelper.enumSelector(Gender);
    this.gradeService.GetAll().subscribe(grades => { this.grades = grades as Grade[]; });
  }


  async onSubmit(form: NgForm) {
    form.value.id = this.student.id;
    if (form.value.id === undefined) {
      await this.studentService.Post(form.value as Student);
    } else {
      await this.studentService.Put(form.value as Student, form.value.id);
    }
    this.dialogRef.close();
  }
}


import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GradeService } from '../../services/grate.service';
import { Grade } from '../../models/grade';
import {MAT_DIALOG_DATA, MatDialogRef, MatOption} from '@angular/material';
import { TeacherService } from '../../services/teacher.service';
import { Teacher } from '../../models/teacher';

@Component({
  selector: 'app-grade',
  templateUrl: './grade.component.html',
  styleUrls: ['./grade.component.css']
})
export class GradeComponent implements OnInit {

  teachers: Teacher[];

  constructor(private gradeService: GradeService,
              private teacherService: TeacherService,
              @Inject(MAT_DIALOG_DATA) public grade: Grade,
              public dialogRef: MatDialogRef<GradeComponent>) {
   }

  async onSubmit(form: NgForm) {
    form.value.id = this.grade.id;
    if (form.value.id === undefined) {
      await this.gradeService.Post(form.value as Grade);
    } else {
      await this.gradeService.Put(form.value as Grade, form.value.id);
    }
    this.dialogRef.close();
  }

  ngOnInit() {
    this.teacherService.GetAll().subscribe(teachers => {this.teachers = teachers as Teacher[]; });
  }

}

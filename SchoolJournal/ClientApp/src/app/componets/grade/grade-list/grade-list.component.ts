import { Component, OnInit } from '@angular/core';
import { GradeService } from '../../../services/grate.service';
import { Observable } from 'rxjs';
import { Grade } from '../../../models/grade';
import { ModalWindowComponent } from '../../../modal-window/modal-window.component';
import { MatDialog } from '@angular/material';
import { GradeComponent } from '../grade.component';

@Component({
  selector: 'app-grade-list',
  templateUrl: './grade-list.component.html',
  styleUrls: ['./grade-list.component.css']
})
export class GradeListComponent implements OnInit {

  displayedColumns = ['name', 'teacher', 'actions'];
  dataSource: Observable<Grade[]>;
  constructor(private gradeService: GradeService, public dialog: MatDialog) {
  }

  addGrade() {
    const grade = new Grade();
    const dialogRef = this.dialog.open(GradeComponent, {data: grade});
    dialogRef.afterClosed().subscribe(() => this.UpdateTable());
  }

  editGrade(grade: Grade) {
    const dialogRef = this.dialog.open(GradeComponent, {data: grade});
    dialogRef.afterClosed().subscribe(() => this.UpdateTable());
  }

  deleteGrade(id: number) {
    const dialogRef = this.dialog.open(ModalWindowComponent);

    dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.gradeService.Delete(id).then(() => this.UpdateTable());
            }
        });
  }

  ngOnInit() {
    this.UpdateTable();
  }

  private UpdateTable() {
      this.dataSource = this.gradeService.GetAll();
  }
}

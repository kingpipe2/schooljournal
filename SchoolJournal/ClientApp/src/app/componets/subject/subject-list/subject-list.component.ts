import { Component, OnInit } from '@angular/core';
import { Subject } from '../../../models/subject';
import { Observable } from 'rxjs';
import { ModalWindowComponent } from '../../../modal-window/modal-window.component';
import { MatDialog } from '@angular/material';
import { SubjectComponent } from '../subject.component';
import { SubjectService } from '../../../services/subject.service';

@Component({
  selector: 'app-subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.css']
})
export class SubjectListComponent implements OnInit {

  displayedColumns = [ 'name', 'actions'];
  dataSource: Observable<Subject[]>;

   constructor(private subjectService: SubjectService, public dialog: MatDialog) {
  }

  addSubject() {
    const subject = new Subject();
    const dialogRef = this.dialog.open(SubjectComponent, {data: subject});
    dialogRef.afterClosed().subscribe(() => this.UpdateTable());
  }

  editSubject(subject: Subject) {
    const dialogRef = this.dialog.open(SubjectComponent, {data: subject});
    dialogRef.afterClosed().subscribe(() => this.UpdateTable());
  }

  deleteSubject(id: number) {
    const dialogRef = this.dialog.open(ModalWindowComponent);
    dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.subjectService.Delete(id).then(answer => this.UpdateTable());
            }
        });
}

  ngOnInit() {
    this.UpdateTable();
  }

  private UpdateTable() {
    this.dataSource = this.subjectService.GetAll();
  }
}

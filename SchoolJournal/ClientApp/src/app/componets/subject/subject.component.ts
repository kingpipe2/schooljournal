import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from '../../models/subject';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SubjectService } from '../../services/subject.service';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  constructor(private subjectService: SubjectService,
              @Inject(MAT_DIALOG_DATA) public subject: Subject,
              public dialogRef: MatDialogRef<SubjectComponent>) {

  }

  async onSubmit(form: NgForm) {
    form.value.id = this.subject.id;
    if (form.value.id === undefined) {
      console.log(form.value);
      await this.subjectService.Post(form.value as Subject);
    } else {
      await this.subjectService.Put(form.value as Subject, form.value.id);
    }
    this.dialogRef.close();
  }

  ngOnInit() {
  }
}

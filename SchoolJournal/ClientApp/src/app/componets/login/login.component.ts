import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { UserLogin } from '../../models/userLogin';
import '../../../assets/login-animation.js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user: UserLogin = <UserLogin>{};

  constructor(private sessionService: SessionService) { }

  public ngOnInit() {
  }
  public ngAfterViewInit() {
    (window as any).initialize();
  }

  public login() {
    this.sessionService.login(this.user);
  }

}

// @Component({
//      selector: 'app-login',
//      templateUrl: './login.component.html',
//      styleUrls: ['./login.component.css']
//    })
// export class LoginComponent {
//   email: string;
//   password: string;

//   ngAfterViewInit() {
//     (window as any).initialize();
//   }

//   login() {
//     console.log(`email: ${this.email} password: ${this.password}`)
//     alert(`Email: ${this.email} Password: ${this.password}`)
//   }
// }

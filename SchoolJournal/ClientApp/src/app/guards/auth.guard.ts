import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/Operators';
import { SessionService } from '../services/session.service';
import { NavbarService } from '../services/navbar.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(public session: SessionService, public router: Router, private navbar: NavbarService) { }

  canActivate(): Observable<boolean> {
    return this.session.isAuthenticated().pipe(map(result => {
      if (!result) {
        this.navbar.hide();
        this.router.navigate(['login']);
      }
      return result;
    }));
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchoolListComponent } from './componets/school/school-list/school-list.component';
import { TeacherListComponent } from './componets/teacher/teacher-list/teacher-list.component';
import { SubjectListComponent } from './componets/subject/subject-list/subject-list.component';
import { GradeListComponent } from './componets/grade/grade-list/grade-list.component';
import { StudentListComponent } from './componets/student/student-list/student-list.component';
import { LoginComponent } from './componets/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { SignUpComponent } from './componets/sign-up/sign-up.component';

const routes: Routes = [
  { path: 'schools', component: SchoolListComponent, canActivate: [AuthGuard] },
  { path: 'teachers', component: TeacherListComponent, canActivate: [AuthGuard] },
  { path: 'subjects', component: SubjectListComponent, canActivate: [AuthGuard] },
  { path: 'grades', component: GradeListComponent, canActivate: [AuthGuard] },
  { path: 'students', component: StudentListComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: '',  pathMatch: 'full', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

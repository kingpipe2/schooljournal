export enum Role {
    admin,
    director,
    teacher,
    student
}

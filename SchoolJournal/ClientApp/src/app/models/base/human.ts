import { Gender } from "./gender";

export class Human{
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    homeAddress: string;
    dateOfBirth: Date;
    gender: Gender;
}
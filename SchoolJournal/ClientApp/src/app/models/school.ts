export class School{
    id: number;
    name: string;
    area: string;
    region: string;
    town: string;
    street: string;
    phone: string;
    indexMail:number;
    mail: string;
}

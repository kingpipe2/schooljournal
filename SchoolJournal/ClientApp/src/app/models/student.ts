import { Human } from './base/human';

export class Student extends Human {
    gradeId: number;
    gradeName: string;
}
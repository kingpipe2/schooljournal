export class Grade{
    id: number;
    name: string;
    teacherId: number;
}
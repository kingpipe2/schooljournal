import { Human } from "./base/human";

export class Teacher extends Human{
    subjectId: number;
    subjectName: string;
    schoolId: number;
    schoolName:string;
}
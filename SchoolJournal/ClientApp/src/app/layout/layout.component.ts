import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NavbarService } from '../services/navbar.service';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {


  constructor(public navbar: NavbarService,
              private sessionService: SessionService,
              private breakpointObserver: BreakpointObserver
              ) {}

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  public logout() {    
    this.sessionService.logout();
  }
}

﻿namespace SchoolJournal.DAL.Enums
{
    public enum Gender
    {
        Man,
        Woman
    }
}

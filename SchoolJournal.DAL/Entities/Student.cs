﻿using SchoolJournal.DAL.Entities.Base;

namespace SchoolJournal.DAL.Entities
{
    public class Student : Human
    {
        public virtual Grade Grade { get; set; }
        public int? GradeId { get; set; }
    }
}

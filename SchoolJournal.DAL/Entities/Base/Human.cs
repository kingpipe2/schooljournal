﻿using SchoolJournal.DAL.Enums;
using System;

namespace SchoolJournal.DAL.Entities.Base
{
    public class Human
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string HomeAddress { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public virtual AppUser User { get; set; }
        public string UserId { get; set; }
    }
}

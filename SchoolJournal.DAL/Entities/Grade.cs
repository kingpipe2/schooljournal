﻿namespace SchoolJournal.DAL.Entities
{
    public class Grade
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Teacher Teacher { get; set; }
        public int TeacherId { get; set; }
    }
}

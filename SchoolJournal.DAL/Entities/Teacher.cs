﻿using SchoolJournal.DAL.Entities.Base;

namespace SchoolJournal.DAL.Entities
{
    public class Teacher : Human
    {
        public virtual Subject Subject { get; set; }
        public int SubjectId { get; set; }
        public virtual School School { get; set; }
        public int SchoolId { get; set; }
    }
}

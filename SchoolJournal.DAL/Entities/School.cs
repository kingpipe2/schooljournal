﻿namespace SchoolJournal.DAL.Entities
{
    public class School
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Area { get; set; }
        public string Region { get; set; }
        public string Town { get; set; }
        public string Street { get; set; }
        public string Phone { get; set; }
        public int IndexMail { get; set; }
        public string Mail { get; set; }
    }
}
